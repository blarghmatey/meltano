---
description: Join the Meltano team

openings:
- title: Technical Marketing Manager
  location: Anywhere, Remote 🌍
  description_url: https://boards.greenhouse.io/meltano/jobs/4075560004
- title: Content Marketing Manager
  location: Anywhere, Remote 🌍
  description_url: https://boards.greenhouse.io/meltano/jobs/4144713004
- title: DataOps Evangelist
  location: Anywhere, Remote 🌍
  description_url: https://boards.greenhouse.io/meltano/jobs/4148045004
- title: Senior Backend Engineer
  location: Anywhere, Remote 🌍
  description_url: https://boards.greenhouse.io/meltano/jobs/4117149004
- title: Senior Backend Engineer
  location: Anywhere, Remote 🌍
  description_url: https://boards.greenhouse.io/meltano/jobs/4117149004
- title: Backend Engineer
  location: Anywhere, Remote 🌍
  description_url: https://boards.greenhouse.io/meltano/jobs/4148049004
- title: Backend Engineer
  location: Anywhere, Remote 🌍
  description_url: https://boards.greenhouse.io/meltano/jobs/4148049004
- title: Senior UI/UX Designer
  location: Anywhere, Remote 🌍
  description_url: https://boards.greenhouse.io/meltano/jobs/4147449004
- title: You need me, but don't know it yet
  location: Anywhere, Remote 🌍
  description_url: https://boards.greenhouse.io/meltano/jobs/4088510004
---

# Join the Meltano team

Our all-remote [team](/team/) and [community](/docs/community.html#slack) of thousands are on a [mission](https://handbook.meltano.com/company/#mission) to **enable everyone to realize the full potential of their data**.
To this end, we are bringing software engineering best practices to data teams in the form of an open source DataOps OS that we [envision](https://handbook.meltano.com/company/#vision) becoming the **foundation of every team's ideal data stack**.

If you want to help data organizations become more effective, collaborative, and confident in the results of their work,
we'd love to hear from you.
If one of the openings below resonates, please apply!
If not, please join our community on <SlackChannelLink>Slack</SlackChannelLink> or [contribute on GitLab](/docs/contributor-guide.html)!

Our [public company handbook](https://handbook.meltano.com) has all the details on
our [values](https://handbook.meltano.com/company/values),
[all-remote work](https://handbook.meltano.com/company/all-remote),
[where we hire](https://handbook.meltano.com/company/all-remote#where-we-hire),
[compensation](https://handbook.meltano.com/peopleops/compensation),
[benefits](https://handbook.meltano.com/peopleops/benefits),
and what it's like to work with us in general.

## Open roles

<TeamGrid :openings="$frontmatter.openings" />
