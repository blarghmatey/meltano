---
description: Learn where to use Meltano, how Meltano is built, and where to get started.
---

# Introduction

Meltano is ELT for the DataOps era:
[open source](https://gitlab.com/meltano/meltano),
[self-hosted](/docs/production.html),
[CLI-first](/docs/command-line-interface.html),
[debuggable](/docs/command-line-interface.html#debugging), and
[extensible](/docs/plugins.html).

To learn **how to use Meltano**, check out the [Getting Started guide](/docs/getting-started.html) and the additional guides and references linked from the Table of Contents in the sidebar.

To learn about **contributing to Meltano**, refer to the [Contributor Guide](/docs/contributor-guide.html).

To learn **how Meltano is made**, check out the [company handbook](https://handbook.meltano.com/) with sections on:
- our company [values](https://handbook.meltano.com/company/values) of empathy, community, sustainability, transparency, iteration, ambition, and accountability,
- our [mission](https://handbook.meltano.com/company/#mission) to enable everyone to realize the full potential of their data,
- our [vision](https://handbook.meltano.com/company/#vision) to become the foundation of every team's ideal data stack,
- the product [strategy](https://handbook.meltano.com/company/#strategy) and [roadmap](https://handbook.meltano.com/product/roadmap) that will get us there, and
- our [history](https://handbook.meltano.com/timeline) from 2018 until today.
